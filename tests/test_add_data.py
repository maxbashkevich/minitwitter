from sqlalchemy import insert
from sqlalchemy.ext.asyncio import AsyncSession
from src.models_schemas.models import ApiKey, User

NAMES = [
    "Luis Suarez",
    "Lionel Messi",
    "Neymar",
    "Sergio Busquets",
    "Dani Alves",
    "Jordi Alba",
    "Gerard Pique",
]
API_KEY = ["test", "test_test"]


async def test_add_users_and_api_key(async_session: AsyncSession) -> None:
    # Добавляем первого пользователя
    stmt_user = insert(User).values(name=NAMES[0])
    await async_session.execute(stmt_user)
    await async_session.commit()
    # Добавляем ключ первого пользователя
    stmt_key = insert(ApiKey).values(user_id=1, api_key=API_KEY[0])
    await async_session.execute(stmt_key)
    await async_session.commit()

    # Добавляем второго пользователя
    stmt_user = insert(User).values(name=NAMES[1]).returning(User)
    user = await async_session.scalar(stmt_user)
    # Добавляем ключ второго пользователя
    stmt_key = insert(ApiKey).values(user_id=1, api_key=API_KEY[1]).returning(ApiKey)
    api_kei = await async_session.scalar(stmt_key)

    await async_session.commit()
    assert user.id == 2 and api_kei.id == 2
