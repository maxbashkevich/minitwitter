import json
import os

import aiofiles
import pytest
from tests.test_add_data import API_KEY, NAMES


@pytest.mark.parametrize("route", ["/api/users/me", "/api/tweets", "/api/users/1"])
async def test_get_route_status(async_client, route):
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.get(route, headers=headers)
    assert resp.status_code == 200


async def test_post_route_tweets(async_client):
    url = "api/tweets"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.post(
        url,
        headers=headers,
        content='{"tweet_data": "какой-то текст", "tweet_media_ids": [1, 2]}',
    )
    assert resp.status_code == 201
    assert json.loads(resp.text) == {"result": True, "tweet_id": 1}


async def test_post_route_medias(async_client):
    current_dir = os.getcwd()
    file_dir = os.path.join(current_dir, "tests/file/test.png")
    url = "/api/medias"
    headers = {"api-key": API_KEY[0]}

    async with aiofiles.open(file_dir, "rb") as f:
        image_data = await f.read()
        files = {"file": ("test.png", image_data, "image/jpeg")}
        resp = await async_client.post(url, headers=headers, files=files)

        assert resp.status_code == 201
        assert json.loads(resp.text) == {"result": True, "media_id": 1}


async def test_post_route_tweet_likes_by_id(async_client) -> None:
    url = "/api/tweets/1/likes"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.post(url, headers=headers)
    assert resp.status_code == 201
    assert json.loads(resp.text) == {"result": True}


async def test_post_route_users_follow_by_id(async_client) -> None:
    url = "/api/users/2/follow"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.post(url, headers=headers)
    assert resp.status_code == 201
    assert json.loads(resp.text) == {"result": True}


async def test_get_route_users_me(async_client) -> None:
    url = "/api/users/me"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.get(url, headers=headers)
    assert resp.status_code == 200
    assert json.loads(resp.text) == {
        "result": True,
        "user": {
            "id": 1,
            "name": NAMES[0],
            "following": [{"id": 2, "name": NAMES[1]}],
            "followers": [],
        },
    }


async def test_get_route_tweets(async_client) -> None:
    url = "/api/tweets"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.get(url, headers=headers)
    assert resp.status_code == 200
    assert json.loads(resp.text) == {
        "result": True,
        "tweets": [
            {
                "id": 1,
                "content": "какой-то текст",
                "attachments": [],
                "author": {"id": 1, "name": NAMES[0]},
                "likes": [{"name": NAMES[0], "user_id": 1}],
            }
        ],
    }


async def test_delete_route_tweets_by_id(async_client) -> None:
    url = "/api/tweets/1"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.delete(url, headers=headers)
    assert resp.status_code == 202
    assert json.loads(resp.text) == {"result": True}


async def test_get_route_users_by_id(async_client) -> None:
    url = "/api/users/2"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.get(url, headers=headers)
    assert resp.status_code == 200
    assert json.loads(resp.text) == {
        "result": True,
        "user": {
            "id": 2,
            "name": NAMES[1],
            "following": [],
            "followers": [{"id": 1, "name": NAMES[0]}],
        },
    }


async def test_delete_route_users_follow_by_id(async_client) -> None:
    url = "/api/users/2/follow"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.delete(url, headers=headers)
    assert resp.status_code == 202
    assert json.loads(resp.text) == {"result": True}
    # Проверка удаления подписки в роуте /api/users/me
    url = "/api/users/me"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.get(url, headers=headers)
    assert json.loads(resp.text) == {
        "result": True,
        "user": {
            "id": 1,
            "name": NAMES[0],
            "following": [],
            "followers": [],
        },
    }


async def test_get_route_users_me_exc(async_client) -> None:
    url = "/api/users/me"
    headers = {"api-key": "no key"}
    resp = await async_client.get(url, headers=headers)
    assert resp.status_code == 401


async def test_post_route_tweets_validation(async_client):
    url = "api/tweets"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.post(
        url,
        headers=headers,
        content='{"data": "bad data}',
    )
    assert resp.status_code == 400


async def test_post_route_medias_exc(async_client):
    url = "/api/medias"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.post(url, headers=headers)
    assert resp.status_code == 400


async def test_post_route_tweet_likes_by_id_exc(async_client) -> None:
    url = "/api/tweets/id/likes"
    headers = {"api-key": API_KEY[0]}
    resp = await async_client.post(url, headers=headers)
    assert resp.status_code == 400
