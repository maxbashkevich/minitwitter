FROM python:3.10-alpine

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

COPY src/ /app/src/

COPY tests/ /app/tests/

COPY pyproject.toml /app/

WORKDIR /app

# Запуск тестов и FastAPI приложения
CMD pytest -vv tests; uvicorn src.main:app --reload --host 0.0.0.0 --port 8000