# Описание сервиса
Бэкенд для сервиса микроблогов.  
Этот проект состоит из нескольких сервисов, включая FastAPI приложение, 
сервер Nginx и базы данных Postgres, работающих внутри контейнеров Docker.

## Пример визуала проекта
![img.png](img.png)


## Структура проекта

- `app`: FastAPI приложение.
- `nginx_server`: Сервер Nginx для маршрутизации и сервировки статических файлов.
- `production_db`: Основная база данных Postgres.
- `test_db`: Тестовая база данных Postgres.


## Используемые технологии

- Docker
- Docker Compose
- FastAPI
- Nginx
- Postgres

## Файловая структура


project_root/  
│  
├── src/  
├── static/  
├── tests/  
├── pyproject.toml  
├── docker-compose.yaml  
├── requirements.txt  
├── Dockerfile  
├── docker-compose.yml  
├── README.md

## Запуск проекта

1. Клонируйте репозиторий:  
`git clone https://gitlab.com/maxbashkevich/minitwitter.git`
2. Постройте и запустите контейнеры:  
`docker-compose up --build`
3. Приложение FastApi будет доступно по адресу [http://localhost:8000](http://localhost:8000), а сервер Nginx по адресу [http://localhost](http://localhost).

## Остановка проекта

Для остановки контейнеров выполните:

`docker-compose down`


