from typing import Sequence

import aiofiles
from fastapi import File, HTTPException, UploadFile
from sqlalchemy import delete, insert, select, update
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import load_only, selectinload
from src.models_schemas.models import (ApiKey, Image, Like, Tweet, User,
                                       user_following)


async def get_user_id_by_api_key(api_key: str, session: AsyncSession) -> int | None:
    """
    Функция для поиска ID пользователя по его ключу
    :param api_key: str
    ключ клиента
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: int | None
    id пользователя
    """
    try:
        query = select(ApiKey).where(ApiKey.api_key == api_key)
        key = await session.execute(query)
        key = key.scalars().one_or_none()
        if key:
            return key.user_id
    except SQLAlchemyError as e:
        await session.rollback()
        raise HTTPException(status_code=400, detail=str(e.__dict__["orig"]))


async def existing_user_by_id(id: int, session: AsyncSession) -> bool:
    """
    Проверка наличия пользователя в базе по ID
    :param id: int
    ID пользователя
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: bool
    флаг
    """
    try:
        select_user_query = select(User).where(User.id == id)
        user = await session.execute(select_user_query)
        user = user.scalar_one_or_none()
        if user:
            return True
        else:
            return False
    except SQLAlchemyError as e:
        await session.rollback()
        raise HTTPException(status_code=400, detail=str(e.__dict__["orig"]))


async def update_tweet_id_in_medias(
    media_ids: list[int],
    tweet_id: int,
    session: AsyncSession,
) -> None:
    """
    Обновление данных о наличии медиафайлов у твита
    :param media_ids: list[int]
    Список из ID медиафайлов
    :param tweet_id: int
    ID твита
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: int | None
    id пользователя
    """
    try:
        for id in media_ids:
            update_tweet_id_query = (
                update(Image).where(Image.id == id).values(tweet_id=tweet_id)
            )
            await session.execute(update_tweet_id_query)
            await session.commit()
    except SQLAlchemyError as e:
        raise HTTPException(status_code=400, detail=str(e.__dict__["orig"]))


async def reformatting_tweets(tweets: Sequence[Tweet]) -> dict:
    """
    Приведение списка всех твитов к нужному формату API
    :param tweets: Sequence[Tweet]
    последовательность твитов
    :return: dict
    Список твиттов в формате API
    """
    new_format_tweets = {
        "result": True,
        "tweets": [
            {
                "content": tweet.content,
                "id": tweet.id,
                "attachments": [
                    f"static/images/{link.filename}" for link in tweet.attachments
                ],
                "author": tweet.author,
                "likes": [
                    {"user_id": like.user_like.id, "name": like.user_like.name}
                    for like in tweet.likes
                ],
            }
            for tweet in tweets
        ],
    }
    return new_format_tweets


async def follow_not_exist(
    user_id: int, following_id: int, session: AsyncSession
) -> dict:
    """
    Создание подписки на пользователя, если такой не существует
    :param user_id: int
    ID подписчика
    :param following_id: int
    ID подписываемого пользователя
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: dict
    ответ формата API
    """
    try:
        select_user_query = select(User).where(User.id == following_id)
        user_to_follow = await session.execute(select_user_query)
        user_to_follow = user_to_follow.scalar_one_or_none()
        if user_to_follow:
            exist_follow = await session.execute(
                select(user_following).filter(
                    user_following.c.user_id == user_id,
                    user_following.c.following_id == following_id,
                )
            )
            exist_follow = exist_follow.scalars().one_or_none()
            if exist_follow:
                return {
                    "result": False,
                    "error_message": f"Пользователь с id={user_id} уже подписан на пользователя с id={following_id}",
                    "error_type": "FollowExist",
                }
            else:
                await create_follow_users(
                    user_id=user_id, following_id=following_id, session=session
                )
                return {"result": True}
        else:
            return {
                "result": False,
                "error_message": f"Пользователь с id={following_id} не найден",
            }
    except SQLAlchemyError as e:
        raise HTTPException(status_code=400, detail=str(e.__dict__["orig"]))


async def get_user_object(user_id: int, session: AsyncSession) -> User | None:
    """
    Получение объекта User по ID
    :param user_id: int
    ID пользователя
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: User | None
    Объект User
    """
    query = (
        select(User)
        .where(User.id == user_id)
        .options(
            load_only(User.id, User.name),
            selectinload(User.followers),
            selectinload(User.following),
        )
    )
    user = await session.execute(query)
    user = user.scalar_one_or_none()
    return user


async def create_follow_users(
    user_id: int, following_id: int, session: AsyncSession
) -> None:
    """
    Добавление подписки в базу данных
    :param user_id: int
    ID подписчика
    :param following_id: int
    ID подписываемого пользователя
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: None
    """
    stmt = insert(user_following).values(user_id=user_id, following_id=following_id)
    await session.execute(stmt)
    await session.commit()


async def delete_follow_users(
    user_id: int, following_id: int, session: AsyncSession
) -> None:
    """
    Удаление подписки из базу данных
    :param user_id: int
    ID подписчика
    :param following_id: int
    ID подписываемого пользователя
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: None
    """

    stmt = delete(user_following).filter(
        user_following.c.user_id == user_id,
        user_following.c.following_id == following_id,
    )
    await session.execute(stmt)
    await session.commit()


async def get_all_tweets(session: AsyncSession) -> dict:
    """
    Загрузка всех твитов из базы данных
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: dict
    список твитов в формате API
    """
    select_query = (
        select(Tweet)
        .options(
            load_only(Tweet.id, Tweet.content),
            selectinload(Tweet.author),
        )
        .options(selectinload(Tweet.likes))
    )
    tweets = await session.execute(select_query)
    tweets = tweets.scalars().all()
    reformat_tweets = await reformatting_tweets(tweets)
    return reformat_tweets


async def create_tweet(
    author_id: int, content: str | int, session: AsyncSession
) -> int:
    """
    Создание твита в базе данных
    :param author_id: int
    ID пользователя, создавшего твит
    :param content: str
    содержание твита
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: int
    ID созданного твита
    """
    stmt = (
        insert(Tweet).values(author_id=author_id, content=content).returning(Tweet.id)
    )
    tweet_id = await session.execute(stmt)
    tweet_id = tweet_id.scalar_one()
    await session.commit()
    return tweet_id


async def get_tweet_by_id(id: int, session: AsyncSession) -> Tweet | None:
    """
    Получение объекта твита по ID
    :param id: int
    ID твита
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: Tweet | None
    объект Tweet
    """
    query_select = select(Tweet).where(Tweet.id == id)
    tweet = await session.execute(query_select)
    tweet = tweet.scalars().one_or_none()
    return tweet


async def delete_tweet_by_id(id: int, session: AsyncSession) -> None:
    """
    Удаление твита по ID
    :param id: int
    ID твита
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return: None
    """
    stmt = delete(Tweet).where(Tweet.id == id)
    await session.execute(stmt)
    await session.commit()


async def add_like_on_tweet(user_id: int, tweet_id: int, session: AsyncSession) -> None:
    """
    Добавление лайка к твиту по ID
    :param user_id: int
    ID пользователя
    :param tweet_id: int
    ID твита
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return:
    """
    await session.execute(insert(Like).values(user_id=user_id, tweet_id=tweet_id))
    await session.commit()


async def delete_like_on_tweet(user_id: int, tweet_id: int, session: AsyncSession):
    """
    Удаление лайка к твиту по ID
    :param user_id: int
    ID пользователя
    :param tweet_id: int
    ID твита
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return:
    """
    await session.execute(
        delete(Like).filter(Like.tweet_id == tweet_id, Like.user_id == user_id)
    )
    await session.commit()


async def get_image(filename: str, session: AsyncSession) -> Image | None:
    """
    Получение объекта Image по имени файла
    :param filename: str
    имя файла
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return Image | None:
    Объект Image
    """
    query = select(Image).where(Image.filename == filename)
    image = await session.execute(query)
    image = image.scalars().one_or_none()
    return image


async def add_image_to_db(filename: str, session: AsyncSession) -> int:
    """
    Добавление картинки в базу данных
    :param filename: str
    имя файла
    :param session: AsyncSession
    асинхронная сессия для работы с базой данных
    :return int:
    ID созданной картинки
    """
    query = insert(Image).values(filename=filename).returning(Image.id)
    image_id = await session.execute(query)
    await session.commit()
    image_id = image_id.scalar_one()
    return image_id


async def save_image(out_file_path: str, file: UploadFile = File(...)) -> None:
    """
    Локальное сохранение принятой картинки
    :param out_file_path:
    путь для сохранения файла
    :param file:
    принятая картинка
    :return: None
    """
    async with aiofiles.open(out_file_path, "wb") as out_file:
        content = await file.read()  # async read
        await out_file.write(content)
