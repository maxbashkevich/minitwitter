from typing import List, Optional

from pydantic import BaseModel


class BoolResult(BaseModel):
    result: bool


class BaseTweet(BaseModel):
    tweet_data: str
    tweet_media_ids: Optional[list[int] | None] = None


# Схема автора
class User(BaseModel):
    id: int
    name: str


# Схема лайка
class Like(BaseModel):
    user_id: int
    name: str


# Схема твита
class Tweet(BaseModel):
    id: int
    content: str
    attachments: List[str]
    author: User
    likes: List[Like]


# Основная схема ответа get запроса
class TweetGetResponse(BaseModel):
    result: bool
    tweets: List[Tweet]


# Схема ответа post запроса
class TweetPostResponse(BaseModel):
    result: bool
    tweet_id: int


class MediaPostResponse(BaseModel):
    result: bool
    media_id: int


# Схема для пользователя
class UserInfo(BaseModel):
    id: int
    name: str
    followers: List[User]
    following: List[User]


# Основная схема ответа
class UserInfoResponse(BaseModel):
    result: bool
    user: UserInfo
