from sqlalchemy import (Column, ForeignKey, Integer, String, Table,
                        UniqueConstraint)
from sqlalchemy.orm import relationship
from src.database import Base


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    tweets = relationship("Tweet", back_populates="author")
    likes = relationship("Like", back_populates="user_like", lazy="select")
    following = relationship(
        "User",
        secondary="user_following",
        primaryjoin="User.id==user_following.c.user_id",
        secondaryjoin="User.id==user_following.c.following_id",
        cascade="all",
        lazy="selectin",
    )
    followers = relationship(
        "User",
        secondary="user_following",
        primaryjoin="User.id==user_following.c.following_id",
        secondaryjoin="User.id==user_following.c.user_id",
        cascade="all",
        lazy="selectin",
    )


user_following = Table(
    "user_following",
    Base.metadata,
    Column("user_id", Integer, ForeignKey(User.id), primary_key=True),
    Column("following_id", Integer, ForeignKey(User.id), primary_key=True),
    UniqueConstraint("following_id", "user_id", name="unique_following"),
)


class ApiKey(Base):
    __tablename__ = "keys"
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(
        Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False
    )
    api_key = Column(String, nullable=False, unique=True)


class Tweet(Base):
    __tablename__ = "tweets"
    id = Column(Integer, primary_key=True, autoincrement=True)
    author_id = Column(
        Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False
    )
    content = Column(String, nullable=False)
    attachments = relationship(
        "Image", back_populates="tweet", cascade="all", lazy="selectin"
    )
    likes = relationship(
        "Like", back_populates="tweet_like", lazy="select", cascade="all"
    )
    author = relationship(
        "User", back_populates="tweets", lazy="selectin", cascade="all"
    )


class Like(Base):
    __tablename__ = "likes"
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    tweet_id = Column(Integer, ForeignKey("tweets.id", ondelete="CASCADE"))
    user_like = relationship("User", back_populates="likes", lazy="selectin")
    tweet_like = relationship("Tweet", back_populates="likes", lazy="selectin")
    __table_args__ = (UniqueConstraint("user_id", "tweet_id", name="user_tweet_uc"),)


class Image(Base):
    __tablename__ = "images"
    id = Column(Integer, primary_key=True, autoincrement=True)
    filename = Column(String(100), nullable=False)
    tweet_id = Column(
        Integer, ForeignKey("tweets.id", ondelete="CASCADE"), nullable=True
    )
    tweet = relationship("Tweet", back_populates="attachments")
