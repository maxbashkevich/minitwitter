from contextlib import asynccontextmanager

from fastapi import FastAPI, HTTPException
from fastapi.exceptions import RequestValidationError
from sqlalchemy import insert, select
from src.database import async_session, init_models
from src.logger_creator import logger
from src.models_schemas.models import ApiKey, Like, Tweet, User, user_following
from src.tweets.router import router as router_tweets
from src.users.router import router as router_users
from starlette.responses import JSONResponse
from tests.test_add_data import NAMES

TEST_CONTENT = [
    "Значимость этих проблем настолько очевидна, что сложившаяся структура организации играет важную роль в формировании системы",
    "Практический опыт показывает, что сложившаяся структура организации способствует подготовке и реализации модели развития!",
    "Дорогие друзья, новая модель организационной деятельности",
]


@asynccontextmanager
async def lifespan(app: FastAPI):
    await init_models()
    logger.info("База данных создана!")
    async with async_session() as session:
        async with session.begin():
            u1 = User(name=NAMES[0])
            u2 = User(name=NAMES[1])
            u3 = User(name=NAMES[2])
            u4 = User(name=NAMES[3])
            session.add_all([u1, u2, u3, u4])
            await session.commit()
        async with session.begin():
            await session.execute(
                insert(user_following).values(user_id=1, following_id=2)
            )
            await session.execute(
                insert(user_following).values(user_id=3, following_id=1)
            )
            await session.commit()

    async with session.begin():
        k1 = ApiKey(user_id=1, api_key="test")
        k2 = ApiKey(user_id=2, api_key="test1")
        k3 = ApiKey(user_id=3, api_key="test2")
        k4 = ApiKey(user_id=3, api_key="test3")
        tw1 = Tweet(
            author_id=1,
            content=TEST_CONTENT[0],
        )
        tw2 = Tweet(
            author_id=2,
            content=TEST_CONTENT[1],
        )
        tw3 = Tweet(author_id=3, content=TEST_CONTENT[2])
        session.add_all([k1, k2, k3, k4, tw1, tw2, tw3])
        await session.commit()

    async with session.begin():
        keys = await session.execute(select(ApiKey))
        keys = keys.scalars().all()
        keys = [key.api_key for key in keys]

    async with session.begin():
        l1 = Like(user_id=1, tweet_id=2)
        l2 = Like(user_id=2, tweet_id=1)
        session.add_all([l1, l2])
        await session.commit()
    logger.info("Тестовые данные добавлены")
    yield


app = FastAPI(title="MiniTwitter API", lifespan=lifespan)

app.include_router(router_users)
app.include_router(router_tweets)


@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse(
        content={
            "result": "false",
            "error_type": "InputError",
            "error_message": exc.detail,
        },
        status_code=exc.status_code,
    )


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    return JSONResponse(
        content={
            "result": "false",
            "error_type": "InputError",
            "error_message": str(exc),
        },
        status_code=400,
    )
