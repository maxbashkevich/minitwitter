from fastapi import APIRouter, Depends, HTTPException, Path, Request
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession
from src.database import get_async_session
from src.logger_creator import logger
from src.models_schemas import schemas
from src.utils import (delete_follow_users, follow_not_exist,
                       get_user_id_by_api_key, get_user_object)

router = APIRouter(prefix="/api/users", tags=["User"])


@router.get(
    "/me",
    summary="Получение пользователем информации о своём профиле",
    description="Пользователь получает перечнь подписчиков и пользователей, на которых он подписан",
    response_model=schemas.UserInfoResponse,
    status_code=200,
)
async def get_user_info(
    request: Request, session: AsyncSession = Depends(get_async_session)
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int = await get_user_id_by_api_key(api_key=api_key, session=session)
        if user_id:
            user = await get_user_object(user_id=user_id, session=session)
            logger.info(
                f"GET Запрос на /users/me от пользователя с id={user_id} обработан"
            )
            return {"result": True, "user": user}
        else:
            message_exc = f"GET Запрос на users/me не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(f"GET запрос на /users/me. Детали: {str(e.__dict__['orig'])}")
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.get(
    "/{id}",
    summary="Получение информации о пользователе по его ID",
    description="Получение по ID пользователя его перечнь подписчиков и пользователей, на которых он подписан",
    response_model=schemas.UserInfoResponse,
    status_code=200,
)
async def get_user_by_id(
    request: Request,
    id: int = Path(..., title="User ID", ge=1),
    session: AsyncSession = Depends(get_async_session),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int = await get_user_id_by_api_key(api_key=api_key, session=session)
        if user_id:
            user = await get_user_object(user_id=id, session=session)
            if user:
                logger.info(
                    f"GET Запрос на /users/{id} от пользователя с id={user_id} обработан"
                )
                return {"result": True, "user": user}
            else:
                message_exc = f"GET Запрос на /users/{id} не обработан. Пользователь с id={id} не найден"
                logger.warning(message_exc)
                raise HTTPException(
                    status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
                )
        else:
            message_exc = f"GET Запрос на /users/{id} не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(f"GET запрос на /users/{id}. Детали: {str(e.__dict__['orig'])}")
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.post(
    "/{id}/follow",
    summary="Подписка на пользователя по ID",
    description="Пользователь добавляет подписку на другого пользователя по его ID",
    response_model=schemas.BoolResult,
    status_code=201,
)
async def following_on_user_by_id(
    request: Request,
    id: int = Path(..., title="User ID", ge=1),
    session: AsyncSession = Depends(get_async_session),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int | None = await get_user_id_by_api_key(
            api_key=api_key, session=session
        )
        if user_id:
            result = await follow_not_exist(
                user_id=user_id, following_id=id, session=session
            )
            if result["result"]:
                logger.info(
                    f"POST Запрос на /users/{id}/follow от пользователя с id={user_id} обработан. Данные добавлены"
                )
                return result
            else:
                message_exc = f"POST Запрос на /users/{id}/follow не обработан. {result['error_message']}"
                logger.warning(message_exc)
                raise HTTPException(
                    status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
                )
        else:
            message_exc = f"POST Запрос на /users/{id}/follow не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(
            f"POST Запрос на /users/{id}/follow. Детали: {str(e.__dict__['orig'])}"
        )
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.delete(
    "/{id}/follow",
    summary="Отписка от пользователя по ID",
    description="Пользователь удаляет подписку на другого пользователя по его ID",
    response_model=schemas.BoolResult,
    status_code=202,
)
async def unfollowing_on_user_by_id(
    request: Request,
    id: int = Path(..., title="User ID", ge=1),
    session: AsyncSession = Depends(get_async_session),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int | None = await get_user_id_by_api_key(
            api_key=api_key, session=session
        )
        if user_id:
            result = await follow_not_exist(
                user_id=user_id, following_id=id, session=session
            )
            if not result["result"] and result["error_type"] == "FollowExist":
                await delete_follow_users(
                    user_id=user_id, following_id=id, session=session
                )
                logger.info(
                    f"DELETE Запрос на /users/{id}/follow от пользователя с id={user_id} обработан. Данные удалены"
                )
                return {"result": True}
            else:
                message_exc = f"DELETE Запрос на /users/{id}/follow не обработан. Пользователь с id={user_id} не подписан на пользователя с id={id}"
                logger.warning(message_exc)
                raise HTTPException(
                    status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
                )
        else:
            message_exc = f"DELETE Запрос на /users/{id}/follow не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(
            f"DELETE Запрос на /users/{id}/follow. Детали: {str(e.__dict__['orig'])}"
        )
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )
