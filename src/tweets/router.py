from fastapi import APIRouter, Depends, File, HTTPException, Path, Request, UploadFile
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession
from src.database import get_async_session
from src.logger_creator import logger
from src.models_schemas import schemas
from src.models_schemas.models import Tweet
from src.utils import (
    add_image_to_db,
    add_like_on_tweet,
    create_tweet,
    delete_like_on_tweet,
    delete_tweet_by_id,
    get_all_tweets,
    get_image,
    get_tweet_by_id,
    get_user_id_by_api_key,
    save_image,
    update_tweet_id_in_medias,
)

router = APIRouter(prefix="/api", tags=["Tweet"])

ERROR_ANSWER = {
    "result": False,
    "error_type": "ServerError",
    "error_message": "Ошибка на стороне сервера",
}


@router.get(
    "/tweets",
    summary="Получение ленты с твитами",
    description="Получение ленты с твитами, с их id, текстом контента, ссылками на вложенные картинки, автором, и лайками",
    response_model=schemas.TweetGetResponse,
    status_code=200,
)
async def get_tweet(
    request: Request, session: AsyncSession = Depends(get_async_session)
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int | None = await get_user_id_by_api_key(
            api_key=api_key, session=session
        )
        if user_id:
            tweets = await get_all_tweets(session=session)
            logger.info(
                f"GET Запрос на на /tweets от пользователя с id={user_id} обработан"
            )
            return tweets
        else:
            message_exc = f"GET Запрос на /tweets не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(f"GET запрос на /tweets. Детали: {str(e.__dict__['orig'])}")
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.post(
    "/tweets",
    summary="Создание нового твита",
    description="Создание нового твита с информацией: текст твита и список из id вложенных картинок",
    response_model=schemas.TweetPostResponse,
    status_code=201,
)
async def add_tweet(
    tweet: schemas.BaseTweet,
    request: Request,
    session: AsyncSession = Depends(get_async_session),
) -> dict:
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int | None = await get_user_id_by_api_key(
            api_key=api_key, session=session
        )
        if user_id:
            tweet_id: int = await create_tweet(
                author_id=user_id, content=tweet.tweet_data, session=session
            )
            media_ids: list = tweet.tweet_media_ids
            if media_ids:
                await update_tweet_id_in_medias(
                    media_ids=media_ids, tweet_id=tweet_id, session=session
                )
            logger.info(
                f"POST Запрос на /tweets от пользователя с id={user_id} обработан, данные добавлены"
            )
            return {"result": True, "tweet_id": tweet_id}
        else:
            message_exc = f"POST Запрос на /tweets не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(f"POST запрос на /tweets. Детали: {str(e.__dict__['orig'])}")
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.delete(
    "/tweets/{id}",
    summary="Удаление твита по его ID",
    description="Удаление твита по его ID. Твит может удалить только его автор.",
    response_model=schemas.BoolResult,
    status_code=202,
)
async def delete_tweet(
    request: Request,
    session: AsyncSession = Depends(get_async_session),
    id: int = Path(..., title="Id of the tweet", ge=1),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int | None = await get_user_id_by_api_key(
            api_key=api_key, session=session
        )
        if user_id:
            tweet: Tweet = await get_tweet_by_id(id=id, session=session)
            if tweet:
                if tweet.author_id == user_id:
                    await delete_tweet_by_id(id=id, session=session)
                    logger.info(
                        f"DELETE Запрос на /tweets{id} от пользователя с id={user_id} обработан, данные удалены"
                    )
                    return {"result": True}
                else:
                    message_exc = f"DELETE Запрос на /tweets{id}. Пользователь с id = {user_id} не является автором твита с id = {id}"
                    logger.warning(message_exc)
                    raise HTTPException(
                        status_code=403,
                        detail="Запрос не обработан. Ошибка на стороне сервера",
                    )
            else:
                message_exc = f"DELETE Запрос на /tweets{id} не обработан. Пользователь с id = {user_id}. Твит с id = {id} не найден"
                logger.warning(message_exc)
                raise HTTPException(
                    status_code=401,
                    detail="Запрос не обработан. Ошибка ввода данных",
                )
        else:
            message_exc = f"DELETE Запрос на /tweets{id} не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(
            f"DELETE запрос на /tweets/{id}. Детали: {str(e.__dict__['orig'])}"
        )
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.post(
    "/tweets/{id}/likes",
    summary="Создание лайка к твиту по его ID",
    description="Пользователь добавляет лайк к существующему твиту по его ID",
    response_model=schemas.BoolResult,
    status_code=201,
)
async def like_tweet_by_id(
    request: Request,
    id: int = Path(..., title="Tweet ID", ge=1),
    session: AsyncSession = Depends(get_async_session),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int = await get_user_id_by_api_key(api_key=api_key, session=session)
        if user_id:
            tweet: Tweet = await get_tweet_by_id(id=id, session=session)
            if tweet:
                await add_like_on_tweet(user_id=user_id, tweet_id=id, session=session)
                logger.info(
                    f"POST Запрос на /tweets{id}/likes от пользователя с id={user_id} обработан, данные добавлены"
                )
                return {"result": True}
            else:
                message_exc = f"POST Запрос на /tweets{id}/likes не обработан. Пользователь с id = {user_id}. Твит с id = {id} не найден"
                logger.warning(message_exc)
                raise HTTPException(
                    status_code=401,
                    detail="Запрос не обработан. Ошибка ввода данных",
                )
        else:
            message_exc = f"POST Запрос на /tweets{id}/likes не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(
            f"POST запрос на /tweets/{id}/likes. Детали: {str(e.__dict__['orig'])}"
        )
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.delete(
    "/tweets/{id}/likes",
    summary="Удаление лайка к твиту по его ID",
    description="Пользователь удаляет лайк к существующему твиту по его ID",
    response_model=schemas.BoolResult,
    status_code=202,
)
async def following_on_user_by_id(
    request: Request,
    id: int = Path(..., title="Tweet ID", ge=1),
    session: AsyncSession = Depends(get_async_session),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id = await get_user_id_by_api_key(api_key=api_key, session=session)
        if user_id:
            tweet: Tweet = await get_tweet_by_id(id=id, session=session)
            if tweet:
                await delete_like_on_tweet(
                    user_id=user_id, tweet_id=id, session=session
                )
                logger.info(
                    f"DELETE Запрос на /tweets{id}/likes от пользователя с id={user_id} обработан, данные удалены"
                )
                return {"result": True}
            else:
                message_exc = f"DELETE Запрос на /tweets{id}/likes не обработан. Пользователь с id = {user_id}. Твит с id = {id} не найден"
                logger.warning(message_exc)
                raise HTTPException(
                    status_code=401,
                    detail="Запрос не обработан. Ошибка ввода данных",
                )
        else:
            message_exc = f"DELETE Запрос на /tweets{id}/likes не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(
            f"DELETE запрос на /tweets/{id}/likes. Детали: {str(e.__dict__['orig'])}"
        )
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )


@router.post(
    "/medias",
    summary="Добавление картинки к твиту",
    description="Загрузка через форму картинки к твиту",
    response_model=schemas.MediaPostResponse,
    status_code=201,
)
async def save_upload_file(
    request: Request,
    file: UploadFile = File(...),
    session: AsyncSession = Depends(get_async_session),
):
    try:
        api_key: str = request.headers.get("api-key")
        user_id: int | None = await get_user_id_by_api_key(
            api_key=api_key, session=session
        )
        if user_id:
            filename = f"{api_key}_{file.filename}"
            image = await get_image(filename=filename, session=session)
            if not image:
                image_id = await add_image_to_db(filename=filename, session=session)
                out_file_path = f"src/medias/{api_key}_{file.filename}"
                await save_image(out_file_path=out_file_path, file=file)
                logger.info(
                    f"POST Запрос на /medias от пользователя с id={user_id} обработан, данные добавлены"
                )
                return {"result": True, "media_id": image_id}
            else:
                logger.info(
                    f"POST Запрос на /medias от пользователя с id={user_id} обработан"
                )
                return {"result": True, "media_id": image.id}
        else:
            message_exc = f"POST Запрос на /medias не обработан. Пользователь с ключом <{api_key}> не найден"
            logger.warning(message_exc)
            raise HTTPException(
                status_code=401, detail="Запрос не обработан. Ошибка ввода данных"
            )
    except SQLAlchemyError as e:
        logger.error(f"POST запрос на /medias. Детали: {str(e.__dict__['orig'])}")
        raise HTTPException(
            status_code=400, detail="Запрос не обработан. Ошибка на стороне сервера"
        )
