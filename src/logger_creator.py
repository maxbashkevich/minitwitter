import sys

from loguru import logger


def get_logger() -> logger:
    """
    Функция для получения объекта логгера
    :return: logger
    логгер
    """
    logger.remove()
    logger.add(
        sys.stdout,
        format="<level>FastApi_APP</level> "
        "<blue>{time:YYYY-MM-DD HH:mm:ss}</blue> "
        "<level>{level} "
        "{message}</level>",
    )
    logger.add(
        "./logs/logs.log",
        format="{time:YYYY-MM-DD HH:mm:ss} {level} {message}",
        level="INFO",
        rotation="5 MB",
    )
    return logger


logger = get_logger()
